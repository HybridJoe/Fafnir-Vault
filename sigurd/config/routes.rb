Rails.application.routes.draw do
  devise_for :clients
  root 'home#index'
  get 'home/joke'
end
