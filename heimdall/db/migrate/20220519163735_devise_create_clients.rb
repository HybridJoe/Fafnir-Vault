class DeviseCreateClients < ActiveRecord::Migration[7.0]
  def change
    create_table :clients do |t|
      t.integer :account
      t.string :username
      t.string :email,              null: false, default: ""
      t.string :encrypted_password, null: false, default: ""

      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      t.datetime :remember_created_at

      t.timestamps null: false
    end

    add_index :clients, :email,                unique: true
    add_index :clients, :reset_password_token, unique: true
    add_index :clients, :account,              unique: true
    # add_index :clients, :unlock_token,         unique: true
  end
end
