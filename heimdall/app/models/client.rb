class Client < ApplicationRecord
    validates :email, uniqueness: true
    validates :account, uniqueness: true
    devise :database_authenticatable, :registerable,
           :recoverable, :rememberable, :validatable
end
