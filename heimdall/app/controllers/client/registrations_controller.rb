# frozen_string_literal: true

class Client::RegistrationsController < Devise::RegistrationsController
   before_action :configure_sign_up_params, only: [:username, :account]
   before_action :configure_permitted_parameters, if: :devise_controller?

   def create
    @clients = Client.new(client_params)
  
      if @clients.save
        redirect_to clientinfo_path, notice: "Cliente cadastrado com sucesso!"
      else
       redirect_to new_client_registration_path, alert: "Algo deu errado, verifique as informações do cadastro."
    end
  end

   protected

  def configure_sign_up_params
     devise_parameter_sanitizer.permit(:sign_up, keys: [:username, :account])
   end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username, :account])
    end

  def client_params
    params.require(:client).permit(:username, :account, :email, :password, :password_confirmation)
    end

end
