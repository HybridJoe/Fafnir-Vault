Rails.application.routes.draw do
  devise_for :clients, controllers: {registrations: 'client/registrations'}
  devise_for :users

  root "home#index"
  get "/client/registerdone", :controller => "client", as: "clientinfo"
end
